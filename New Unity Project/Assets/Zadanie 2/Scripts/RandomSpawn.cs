﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class RandomSpawn : MonoBehaviour
{
    public GameObject point;

    public float spawnRate;
    private float nextTimeToSpawn = .5f;  
    void Start()
    {
    }
    private void Update()
    {
        SpawnPoint();
    }

    private void SpawnPoint()
    {       
        if (Time.time >= nextTimeToSpawn)
        {
            float spawnY = Random.Range(-5f, 5f);
            float spawnX = Random.Range(-9f, 9f);
            Vector2 spawnPosition = new Vector2(spawnX, spawnY);
            GameObject newBall = ObjectsToPool.SharedInstance.GetPooledObject("SmallBal");
            newBall.transform.position = spawnPosition;
            newBall.SetActive(true);          
            nextTimeToSpawn = Time.time + spawnRate;
        }
    }

   
}
