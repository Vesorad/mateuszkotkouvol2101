﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class PoolObject
{

    public string name;
    public GameObject objectToPool;
    public int amountToPool;
    public List<GameObject> pooledObjects;

}
