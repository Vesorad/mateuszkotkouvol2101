﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class ObjectsToPool : MonoBehaviour
{
    Gravity gravity;
    public static ObjectsToPool SharedInstance;
    public PoolObject[] poolObject;

    private void Awake()
    {
        SharedInstance = this;
    }
    ///////////////////////////////////  SPAWN WSZYSTKICH OBIEKTÓW PRZY ODPALANIU SCENY
    private void Start()
    {
        foreach (var item in poolObject)
        {
            item.pooledObjects = new List<GameObject>();
            GameObject tmp;
            for (int i = 0; i < item.amountToPool; i++)
            {
                tmp = Instantiate(item.objectToPool);
                tmp.SetActive(false);
                item.pooledObjects.Add(tmp);
            }
        }
    }
    ///////////////////////////////////  POBIERANIE DANEGO OBIEKTU
    public GameObject GetPooledObject(string name)
    {
        PoolObject thisPooledObject = Array.Find(poolObject, poolObject => poolObject.name == name);

        if (thisPooledObject == null)
        {
            Debug.Log("Nie ma takiego obiektu jak: " + name);
            return null;
        }
        else
        {
            for (int i = 0; i < thisPooledObject.amountToPool; i++)
            {
                if (!thisPooledObject.pooledObjects[i].activeInHierarchy)
                {
                    return thisPooledObject.pooledObjects[i];
                }
            }
            return null;
        }
    }
}

