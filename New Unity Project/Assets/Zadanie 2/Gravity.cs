﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Gravity : MonoBehaviour
{
    public float speed;
    public int nummberBalls=1;

    Gravity[] gravity;
    private void Awake()
    {
        gravity = FindObjectsOfType<Gravity>();
    }
    void Update()
    {
        if (nummberBalls >=50)
        {
            nummberBalls = 1;
            gameObject.transform.localScale = new Vector3(1, 1, 1);
            for (int i = 0; i < 50; i++)
            {
                //NewBalls();
            }
            gameObject.SetActive(false);
        }
    }

    private static void NewBalls()
    {
        float spawnY = Random.Range(-5f, 5f);
        float spawnX = Random.Range(-9f, 9f);
        Vector2 spawnPosition = new Vector2(spawnX, spawnY);
        GameObject newBall = ObjectsToPool.SharedInstance.GetPooledObject("SmallBal");
        newBall.transform.position = spawnPosition;
        newBall.SetActive(true);
    }

    private void OnTriggerStay2D(Collider2D collision)
    {
        transform.position = Vector3.MoveTowards(gameObject.transform.position, collision.transform.position, speed * Time.deltaTime);
        if (transform.position == collision.transform.position)
        {
          var numberEnemyBall = collision.GetComponent<Gravity>().nummberBalls;
            if (numberEnemyBall <= nummberBalls && gameObject.activeSelf)
            {
                numberEnemyBall = CollisonBalls(collision, numberEnemyBall);

            }

        }

    }

    private int CollisonBalls(Collider2D collision, int numberEnemyBall)
    {
        gameObject.transform.localScale = new Vector3(gameObject.transform.localScale.x + (numberEnemyBall * 0.1f), gameObject.transform.localScale.y + (numberEnemyBall * 0.1f), gameObject.transform.localScale.z);
        nummberBalls += numberEnemyBall;
        collision.gameObject.transform.localScale = new Vector3(1, 1, 1);
        numberEnemyBall = 1;
        collision.gameObject.SetActive(false);
        return numberEnemyBall;
    }
}

