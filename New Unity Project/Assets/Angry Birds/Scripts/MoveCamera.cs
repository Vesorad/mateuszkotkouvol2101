﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoveCamera : MonoBehaviour
{
    [SerializeField] Bomb mainObject;
    Vector3 mainObjectPos;
    public SpringJoint2D springJoint;

    private void Awake()
    {
        mainObject = FindObjectOfType<Bomb>();
        springJoint = mainObject.gameObject.GetComponent<SpringJoint2D>();
    }
    void Start()
    {
        
    }
    void Update()
    {
        mainObject = FindObjectOfType<Bomb>();
        springJoint = mainObject.gameObject.GetComponent<SpringJoint2D>();
        MoveCamerToBomb();
    }

    private void MoveCamerToBomb()
    {
        if (springJoint.enabled == false)
        {
            mainObjectPos = mainObject.transform.position;
            transform.position = new Vector3(mainObjectPos.x, mainObjectPos.y, -10);
        }
        if (springJoint.enabled == true)
        {
            transform.position = new Vector3(0,0,-10);
        }
    }
}
