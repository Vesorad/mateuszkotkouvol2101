﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BombManager : MonoBehaviour
{
    public GameObject bombPreFab;
    public GameObject spawnPoint;
    public Bomb bomb;

    void Update()
    {
        bomb = FindObjectOfType<Bomb>();
        if (bomb == null)
        {
            bomb = FindObjectOfType<Bomb>();
        }
    }
    public void SpawnObject()
    {
        var newBomb = Instantiate(bombPreFab);
        newBomb.GetComponent<SpringJoint2D>().connectedBody = spawnPoint.GetComponent<Rigidbody2D>();
    }
}
