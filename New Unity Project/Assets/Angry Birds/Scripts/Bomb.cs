﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bomb : MonoBehaviour
{
    bool Pressed;
   public bool ObjectOff;
    [SerializeField] float timeToHold;

    Rigidbody2D rigidbody2D;
    SpringJoint2D springJoint;
    BombManager bombManager;
    private void Start()
    {
        springJoint = GetComponent<SpringJoint2D>();
        rigidbody2D = GetComponent<Rigidbody2D>();
        bombManager = FindObjectOfType<BombManager>();
    }
    private void Update()
    {
        if (Pressed)
        {
            DragBall();
        } 
    }
    private void OnMouseDown()
    {
        Pressed = true;
        rigidbody2D.isKinematic = true;
    }
    private void OnMouseUp()
    {
        Pressed = false;
        rigidbody2D.isKinematic = false;
        StartCoroutine(Relase());
    }
    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.tag == "Ground")
        {
            StartCoroutine(NextBomb());
        }
    }
    void DragBall()
    {        
        Vector2 mousePos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
        rigidbody2D.position = mousePos;
    }   
    IEnumerator Relase()
    {
        yield return new WaitForSeconds(timeToHold);
        springJoint.enabled = false;
    }
    IEnumerator NextBomb()
    {
        Debug.Log("udezenie");
        yield return new WaitForSeconds(1);
        bombManager.SpawnObject();
        ObjectOff = true;
        Destroy(this);
    }
}
